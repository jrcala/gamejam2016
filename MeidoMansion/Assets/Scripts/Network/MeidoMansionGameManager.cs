﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class MeidoMansionGameManager : NetworkBehaviour {

	[SyncVar]
	public GameStates gameState = GameStates.INITIALIZING;

	[SyncVar]
	public MasterStates masterState = MasterStates.ALIVE;

	[SyncVar]
	public int turn = 0;

	private const int MAX_TURNS = 24;

	public List<Tile> brigLoc;

	public List<GameObject> players = new List<GameObject>();
	public GameObject masterPiece;
	public Grid grid;


	public static MeidoMansionGameManager Instance;
	public static MeidoMansionNetworkManagerScript networkManager;

	private GameStates currentState = GameStates.NONE;

	void Awake() {
		Instance = this;
	}

	// Use this for initialization
	void Start () {
		grid = FindObjectOfType<Grid>();
	}
	
	// Update is called once per frame
	void Update () {
		if(currentState != gameState) {
			Cmd_ProcessState(currentState, gameState);
		}
	}

	public void AddObject(GameObject obj) {
		players.Add(obj);
	}

	[Command]
	public void Cmd_SpawnMaster() {
		networkManager = FindObjectOfType<MeidoMansionNetworkManagerScript>();
		networkManager.SpawnMaster();
	}

	[Command]
	public void Cmd_ProcessState(GameStates previousState, GameStates nextState) {
		Debug.Log("Changing State " + nextState);
		if(nextState == GameStates.TURN_RESOLVE) {
			turn++;
			foreach(GameObject obj in players){
				obj.GetComponent<NetworkPlayerScript>().turn = turn;
			}
			Debug.Log("Check kill ");
			Cmd_CheckIfCanKill();
			Cmd_CheckIfSettingTrap();
			Cmd_CheckIfBrig();

		}
		previousState = nextState;
		currentState = previousState;
	}

	[Command]
	public void Cmd_EndDay(){
		turn = 0;
		foreach(GameObject go in players){
			if(go.GetComponent<NetworkPlayerScript>() != null)
				go.GetComponent<NetworkPlayerScript>().Respawn();
		}
		masterPiece.GetComponent<PlayerController>().Reset();
		Cmd_CheckAllPlayerMove();

	}

	[Command]
	public void Cmd_CheckIfBrig(){
		for(int i = 0; i < players.Count; i++){
			NetworkPlayerScript playerComponent = players[i].GetComponent<NetworkPlayerScript>();
			if(playerComponent.isBrig){
				Cmd_ThrowToBrig(players[i]);
				playerComponent.isBrig = false;
			}
			/*List<int> turnSched = playerComponent.turnSchedule;
			for(int j = 0; j < turnSched.Count;j++){
				int playerTurn = turnSched[j];
				if(playerTurn == this.turn){
					Tile currTile = playerComponent.pieceComponent.GetCurrentTile();
					if(currTile != playerComponent.tileSchedule[j]){
						Cmd_ThrowToBrig(players[i]);
					}

				}
			}*/

		}

	}

	[Command]
	void Cmd_ThrowToBrig(GameObject playerObj){
		Debug.Log(playerObj.name + " was thrown to the brig");
		NetworkPlayerScript playerScript = playerObj.GetComponent<NetworkPlayerScript>();
		Tile brig = brigLoc[Random.Range(0,brigLoc.Count)];
		playerScript.pieceComponent.ForceMoveTo(brig.xGridPos,brig.yGridPos);
	}

	[Command]
	public void Cmd_StartGame() {
		for(int i = 0; i < players.Count; i++) {
			float rand = Random.Range(0, 100);

			if(rand <= 100 / players.Count || i == players.Count - 1) {
				Cmd_SetKiller(players[i]);
				break;
			}
		}

		Cmd_SpawnMaster();

		foreach (GameObject playerObj in players) {
			playerObj.GetComponent<NetworkPlayerScript>().Rpc_StartGame();
		}

		gameState = GameStates.MOVE_SELECTION;
		masterState = MasterStates.ALIVE;
	}

	public bool KillerCanKill() {
		for (int i = 0; i < players.Count; i++) {
			NetworkPlayerPieceHandler playerPiece = players[i].GetComponent<NetworkPlayerScript>().piece.GetComponent<NetworkPlayerPieceHandler>();
			if (players[i].GetComponent<NetworkPlayerScript>().isKiller) {
				return playerPiece.TryKill();
            }
		}

		return false;
	}

	[Command]
	private void Cmd_SetKiller(GameObject obj) {
		obj.GetComponent<NetworkPlayerScript>().isKiller = true;
		Debug.Log(obj.name + " is the killer");
	}

	[ClientRpc]
	public void Rpc_UnsetTrap() {
		for (int i = 0; i < players.Count; i++) {
			NetworkPlayerScript playerNetwork = players[i].GetComponent<NetworkPlayerScript>();
			PlayerController playerController =playerNetwork.piece.GetComponent<PlayerController>();
			NetworkPlayerPieceHandler pieceHandler = playerController.gameObject.GetComponent<NetworkPlayerPieceHandler>();
			if (!playerNetwork.isKiller && playerController.playerMove == MoveDirection.STAY) {
				Tile tile = pieceHandler.GetCurrentTile();
				tile.UnSetTrap();
				tile.HideTrap();
            }
		}
	}

	[Command]
	public void Cmd_CheckIfSettingTrap(){
		for(int i = 0; i < players.Count;i++) {
			Debug.Log("Checking if " + players[i].name + " can kill");
			NetworkPlayerPieceHandler playerPiece = players[i].GetComponent<NetworkPlayerScript>().piece.GetComponent<NetworkPlayerPieceHandler>();
			NetworkPlayerScript playerNetwork = players[i].GetComponent<NetworkPlayerScript>();
			if (playerNetwork.isSettingTrap && playerNetwork.isKiller) {
				Debug.Log(players[i].name + "SETTING TRAP " );
				grid.GetTile(playerPiece.xGridPos,playerPiece.yGridPos).SetTrap();
				playerNetwork.isSettingTrap = false;
			}
		}
	}

	[Command]
	public void Cmd_CheckIfCanKill() {
		for(int i = 0; i < players.Count;i++) {
			Debug.Log("Checking if " + players[i].name + " can kill");
			NetworkPlayerPieceHandler playerPiece = players[i].GetComponent<NetworkPlayerScript>().piece.GetComponent<NetworkPlayerPieceHandler>();
			NetworkPlayerScript playerNetwork = players[i].GetComponent<NetworkPlayerScript>();
			if (playerPiece.TryKill() && playerNetwork.isKiller) {
				Debug.Log(players[i].name + "CAN KILL " );
				if(playerNetwork.isKilling){
					this.Cmd_KillMaster();
					playerNetwork.isKilling = false;
				}
			}
		}
	}

	[Command]
	public void Cmd_RemoveObject(GameObject obj) {
		players.Remove(obj);
	}

	[Command]
	public void Cmd_CheckAllPlayerMove() {
		for(int i = 0; i < players.Count; i++) {
			if (players[i] != null) {
				if (players[i].GetComponent<NetworkPlayerScript>().playerMove == MoveDirection.NONE) {
					Debug.Log("Still waiting for someone");
					return;
				}
			}
			else {
				players.RemoveAt(i);
				Cmd_CheckAllPlayerMove();
			}
		}

		Cmd_CheckIfCanKill();
		Cmd_MoveAll();
		Debug.Log("All players finished selecting moves");
	}

	[Command]
	public void Cmd_CheckAllPlayerAnimationDone() {
		for (int i = 0; i < players.Count; i++) {
			if (players[i] != null) {
				if (players[i].GetComponent<NetworkPlayerScript>().piece.GetComponent<NetworkPlayerPieceHandler>().animationState == TileAnimationStates.ANIMATING) {
					Debug.Log("Still waiting for other animation");
					return;
				}
			}
			else {
				players.RemoveAt(i);
				Cmd_CheckAllPlayerMove();

			}
		}

		gameState = GameStates.MOVE_SELECTION;
		Debug.Log("Animations done");
		if(turn >= MAX_TURNS){
			Cmd_EndDay();
		}
	}

	[Command]
	public void Cmd_MoveAll() {
		int stayed = 0;
		
		for (int i = 0; i < players.Count; i++) {
            players[i].GetComponent<NetworkPlayerScript>().piece.GetComponent<PlayerController>().Rpc_Move(players[i].GetComponent<NetworkPlayerScript>().playerMove);

			if(players[i].GetComponent<NetworkPlayerScript>().playerMove == MoveDirection.STAY) {
				stayed++;
			}
			else {

			}
			players[i].GetComponent<NetworkPlayerScript>().Rpc_UnSetTrap();
			players[i].GetComponent<NetworkPlayerScript>().UnSetTrap();

			players[i].GetComponent<NetworkPlayerScript>().playerMove = MoveDirection.NONE;

		}

		if (masterState == MasterStates.ALIVE) {
			masterPiece.GetComponent<PlayerController>().Rpc_Move();
			if(stayed == players.Count && masterPiece.GetComponent<PlayerController>().GetCurrentMove() == MoveDirection.STAY) {
				gameState = GameStates.MOVE_SELECTION;
			}
		}

		else if(masterState == MasterStates.DEAD && stayed == players.Count) {
			gameState = GameStates.MOVE_SELECTION;
		}

		if(grid != null){
			grid.Refresh();
		}

		gameState = GameStates.TURN_RESOLVE;
	}
	[Command]
	public void Cmd_KillMaster(){
		Debug.Log("Master Has been killed");
		masterState = MasterStates.DEAD;
	}
}
