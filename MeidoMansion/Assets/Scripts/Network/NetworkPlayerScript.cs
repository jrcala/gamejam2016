using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System;

public class NetworkPlayerScript : NetworkBehaviour {
	/// <summary>
	/// The direction the player will move next turn
	/// </summary>
	[SyncVar]
	public MoveDirection playerMove = MoveDirection.NONE;

	[SyncVar]
	public GameObject piece = null;

	[SyncVar]
	public string pieceName = "";

	[SyncVar]
	public Jobs playerJob = Jobs.NONE;

	[SyncVar]
	public bool isKiller = false;

	[SyncVar]
	public bool isKilling = false;

	[SyncVar]
	public bool isSettingTrap = false;

	[SyncVar]
	public bool isBrig = false;

	[SyncVar]
	public int turn = 0;

	public List<string> scheduleList;

	public List<int> turnSchedule;

	public List<Tile> tileSchedule;

	private Grid _grid;


	private Grid grid{
		get{
			if(_grid == null){
				_grid = GameObject.FindObjectOfType<Grid>();
			}
			return _grid;
		}
		set{
			_grid = value;
		}
	}

	public NetworkPlayerPieceHandler pieceComponent {
		get{
			if (piece != null)
				return piece.GetComponent<NetworkPlayerPieceHandler>();
			else return null;
		}
	}


	// Use this for initialization
	void Start () {
		piece = GameObject.Find("PlayerPiece"+gameObject.name);
		if(piece != null)
			piece.GetComponent<NetworkPlayerPieceHandler>().Init(this);
		if(isLocalPlayer){
			grid = GameObject.FindObjectOfType<Grid>();
			grid.SetTargetPlayer(this);
			grid.Refresh();
			if(piece != null)
				GameObject.FindObjectOfType<SmoothCamera2D>().target = piece.transform;
			if(piece != null)
				GenerateSchedule();
		}
	}

	void GenerateSchedule(){
		string result = scheduleList[UnityEngine.Random.Range(0, scheduleList.Count)];
		int[] turnList = Array.ConvertAll<string,int>(result.Split(','), s=>int.Parse(s));
		int currX = this.pieceComponent.xGridPos;
		int currY = this.pieceComponent.yGridPos;
		int prevTurn = 0;
		List<Tile> targetTiles = new List<Tile>();
		Tile currTile = grid.GetTile(currX, currY);
		foreach(int turn in turnList){
			int maxDist = turn-prevTurn - UnityEngine.Random.Range(1,5);
			List<Tile> explored = new List<Tile>();

			List<Tile> tileList = currTile.TraversableTiles();
			tileList.RemoveAll(t=> t.turnSchedule.Count > 0);
			if(tileList.Count > 0 ){
				for(int i =0; i < maxDist; i++){
					explored.Add(currTile);
					currTile = tileList[UnityEngine.Random.Range(0, tileList.Count)];	
					if(i == maxDist - 1 || UnityEngine.Random.Range(0,maxDist) == 0){
						
						break;
					}
					tileList = currTile.TraversableTiles();
					foreach(Tile removeTile in explored){
						tileList.Remove(removeTile);
					}
					tileList.RemoveAll(t=> t.turnSchedule.Count > 0);
					if(tileList.Count == 0){
						break;
					}

				}
			}
			currTile.AddTurnSched(turn);
			targetTiles.Add(currTile);


			prevTurn = turn;
			
		}
		this.turnSchedule = new List<int>(turnList);
		this.tileSchedule = targetTiles;
	}

	// Update is called once per frame
	void Update () {
		if(isLocalPlayer){
			GameUIManager.Instance.SetKillerUIVisibility(isKiller);
			if(this.isKiller){
				this.tileSchedule.ForEach(t=>t.UnSetSched());
				this.tileSchedule.Clear();
				this.turnSchedule.Clear();
			}
		}
		if (piece == null) {
			piece = GameObject.Find("PlayerPiece" + gameObject.name);
			if (piece != null) {
				piece.GetComponent<NetworkPlayerPieceHandler>().Init(this);
				if(isLocalPlayer){
					GameObject.FindObjectOfType<SmoothCamera2D>().target = piece.transform;
					GenerateSchedule();
				}
			}
		
		}

		gameObject.name = pieceName;

		GameUIManager.Instance.startButton.SetActive(isServer && MeidoMansionGameManager.Instance.gameState == GameStates.INITIALIZING);

		if (isLocalPlayer) {

			if (grid.HasTargetPlayer()) {

				grid.SetTargetPlayer(this);
				grid.Refresh();
			}

			if(MeidoMansionGameManager.Instance.gameState == GameStates.MOVE_SELECTION){
				if (Input.GetKeyDown(KeyCode.RightArrow)) {
					MovePlayer(MoveDirection.RIGHT);
				}
				else if (Input.GetKeyDown(KeyCode.LeftArrow)) {
					MovePlayer(MoveDirection.LEFT);
				}
				else if (Input.GetKeyDown(KeyCode.UpArrow)) {
					MovePlayer(MoveDirection.UP);
				}
				else if (Input.GetKeyDown(KeyCode.DownArrow)) {
					MovePlayer(MoveDirection.DOWN);
				}
				else if (Input.GetKeyDown(KeyCode.Space)) {
					MovePlayer(MoveDirection.STAY);
				}

				/*if (isKiller) {
					GameUIManager.Instance.SetKillerUIVisibility(piece.GetComponent<NetworkPlayerPieceHandler>().TryKill());
				}
				else {
					GameUIManager.Instance.SetKillerUIVisibility(false);
				}*/
			}
		}
		
	}

	public void MovePlayer(MoveDirection direction){
		for(int j = 0; j < turnSchedule.Count;j++){
			int playerTurn = turnSchedule[j];
			if(playerTurn == turn){
				Tile currTile = pieceComponent.GetCurrentTile();
				if(currTile != tileSchedule[j]){
					isBrig = true;
					Cmd_SetBrig();
				}

			}
		}
		Cmd_MovePlayer(direction);
	}


	public void Respawn(){
		pieceComponent.Respawn();
	}

	[Command]
	public void Cmd_SetBrig(){
		isBrig = true;
	}

	[Command]
	public void Cmd_MovePlayer(MoveDirection direction) {
		if (MeidoMansionGameManager.Instance.gameState == GameStates.MOVE_SELECTION) {
			playerMove = direction;
			MeidoMansionGameManager.Instance.Cmd_CheckAllPlayerMove();
		}
	}

	[Command]
	public void Cmd_SetTrap(){
		if (MeidoMansionGameManager.Instance.gameState == GameStates.MOVE_SELECTION) {
			this.playerMove = MoveDirection.STAY;
			isSettingTrap = true;
			MeidoMansionGameManager.Instance.Cmd_CheckIfSettingTrap();
			MeidoMansionGameManager.Instance.Cmd_CheckAllPlayerMove();
		}
	}



	public void SetTrap(){
		Cmd_SetTrap();
		NetworkPlayerPieceHandler pieceHandler = this.piece.GetComponent<NetworkPlayerPieceHandler>();
		grid.GetTile(pieceHandler.xGridPos,pieceHandler.yGridPos).ShowTrap();
	}

	[ClientRpc]
	public void Rpc_UnSetTrap() {
		Debug.Log("Unsetting trap");
		NetworkPlayerScript[] scripts = FindObjectsOfType<NetworkPlayerScript>();
		NetworkPlayerPieceHandler pieceNet = piece.GetComponent<NetworkPlayerPieceHandler>();
		for (int i = 0; i < scripts.Length; i++) {
			if (scripts[i].playerMove == MoveDirection.STAY) {
				Tile t = scripts[i].piece.GetComponent<NetworkPlayerPieceHandler>().GetCurrentTile();
				Debug.Log("Removing " + t.xGridPos + " " + t.yGridPos);
				pieceNet.UnsetTrap(t.xGridPos, t.yGridPos);
			}
		}
	}

	[ClientRpc]
	public void Rpc_StartGame() {
		MainGameUIScript.Instance.SetSplashScreenVisibility(false);


	}

	public void UnSetTrap() {
		Debug.Log("Unsetting trp");
		NetworkPlayerScript[] scripts = FindObjectsOfType<NetworkPlayerScript>();
		NetworkPlayerPieceHandler pieceNet = piece.GetComponent<NetworkPlayerPieceHandler>();
		for (int i = 0; i < scripts.Length; i++) {
			PlayerController cont = scripts[i].piece.GetComponent<PlayerController>();
            if (scripts[i].playerMove == MoveDirection.STAY && !scripts[i].isKiller) {
				Tile t = scripts[i].piece.GetComponent<NetworkPlayerPieceHandler>().GetCurrentTile();
				Debug.Log("Removing " + t.xGridPos + " " + t.yGridPos);
				pieceNet.UnsetTrap(t.xGridPos, t.yGridPos);
			}
		}
	}

	[Command]
	public void Cmd_KillMaster(){
		if (MeidoMansionGameManager.Instance.gameState == GameStates.MOVE_SELECTION) {
			this.playerMove = MoveDirection.STAY;
			isKilling = true;
			MeidoMansionGameManager.Instance.Cmd_CheckAllPlayerMove();
		}
	}

	[Command]
	public void Cmd_AssignJob(Jobs job) {
		playerJob = job;
	}

}
