﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using DG.Tweening;

public class NetworkPlayerPieceHandler : NetworkBehaviour {
	public Grid grid;

	[SyncVar]
	public int xGridPos;
	[SyncVar]
	public int yGridPos;

	[SyncVar]
	public int startingPosX;

	[SyncVar]
	public int startingPosY;


	[SyncVar]
	public string pieceName = "";

	public bool isMaster = false;

	public float moveDuration = 1.0f;
	public TileAnimationStates animationState = TileAnimationStates.STATIONARY;

	private PlayerController controller;
	private NetworkPlayerScript playerScript;
	private SpriteRenderer spriteRenderer;

	private MeidoMansionNetworkManagerScript networkManager;

	public void Init(NetworkPlayerScript player) {
		playerScript = player;
		startingPosX = xGridPos;
		startingPosY = yGridPos;
	}

	public void Respawn(){
		DOTween.CompleteAll();
		xGridPos = startingPosX;
		yGridPos = startingPosY;
		if(grid == null){
			grid = GameObject.FindObjectOfType<Grid>();
		}
		this.transform.position = grid.GetTilePos(xGridPos, yGridPos);
	}

	void Start() {
		//contrller = GetComponent<PlayerController>();
		if (grid == null) {
			grid = FindObjectOfType<Grid>();
		}
		controller = GetComponent<PlayerController>();
		spriteRenderer = GetComponentInChildren<SpriteRenderer>();
		networkManager = FindObjectOfType<MeidoMansionNetworkManagerScript>();
		this.transform.position = grid.GetTilePos(xGridPos, yGridPos);
	}
	public void SetSpawn(Vector2 spawn){
		startingPosX = (int)spawn.x;
		startingPosY = (int)spawn.y;
	}

	public void Update() {
		//Branch comii
		gameObject.name = pieceName;
		if (playerScript != null) {
			//spriteRenderer.color = networkManager.PlayerColors[(int)playerScript.playerJob];
			spriteRenderer.sprite = networkManager.characterSprites[(int)playerScript.playerJob];
			if(playerScript.isLocalPlayer && playerScript.isKiller) {
				//spriteRenderer.color = networkManager.PlayerColors[4];
			}
		}

		if (pieceName.Contains("Master")) {
			//spriteRenderer.color = networkManager.PlayerColors[5];
			spriteRenderer.sprite = networkManager.characterSprites[4];
		}
	}

	public void MoveTo(int x, int y) {
		if (grid.IsValid(x, y)) {
			if (x < xGridPos) {
				if (!grid.GetTile(xGridPos, yGridPos).canGoLeft) {
					return;
				}
			}

			if (x > xGridPos) {
				if (!grid.GetTile(xGridPos, yGridPos).canGoRight) {
					return;
				}
			}

			if (y < yGridPos) {
				if (!grid.GetTile(xGridPos, yGridPos).canGoUp) {
					return;
				}
			}

			if (y > yGridPos) {
				if (!grid.GetTile(xGridPos, yGridPos).canGoDown) {
					return;
				}
			}
			ForceMoveTo(x,y);
		}
	}
	public void ForceMoveTo(int x, int y){
		Vector2 tilePos = grid.GetTilePos(x, y);
		Tweener MoveAnimation = this.transform.DOMove(tilePos, moveDuration);
		MoveAnimation.OnStart(delegate() {
			animationState = TileAnimationStates.ANIMATING;
		});
		MoveAnimation.OnComplete(delegate () {
			animationState = TileAnimationStates.STATIONARY;
			controller.Cmd_PlayerMoveDone();
		});
		xGridPos = x;
		yGridPos = y;
	}
		

	public bool TryKill() {
		Tile currTile = GetCurrentTile();
		return currTile.isGoalTile;
	}

	public Tile GetCurrentTile() {
		return grid.GetTile(xGridPos, yGridPos);
	}

	[ContextMenu("Up")]
	public void MoveUp() {
		MoveTo(xGridPos, yGridPos - 1);
	}

	[ContextMenu("Left")]
	public void MoveLeft() {
		MoveTo(xGridPos - 1, yGridPos);
	}

	[ContextMenu("Right")]
	public void MoveRight() {
		MoveTo(xGridPos + 1, yGridPos);
	}

	[ContextMenu("Down")]
	public void MoveDown() {
		MoveTo(xGridPos, yGridPos + 1);
	}

	public void UnsetTrap(int x, int y) {
		Tile t = grid.GetTile(x, y);
		t.UnSetTrap();
		t.HideTrap();
	}

	public void Stay() {
		animationState = TileAnimationStates.STATIONARY;
		controller.Cmd_PlayerMoveDone();

		if (!playerScript.isKiller) {
			Tile tile = GetCurrentTile();
			tile.UnSetTrap();
			//MeidoMansionGameManager.Instance.Rpc_UnsetTrap();
        }
	}

}
