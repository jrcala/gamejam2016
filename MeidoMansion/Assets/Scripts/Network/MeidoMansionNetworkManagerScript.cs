﻿using UnityEngine;
using UnityEngine.Sprites;
using System.Collections;
using UnityEngine.Networking;


public class MeidoMansionNetworkManagerScript : NetworkManager {

	public static MeidoMansionNetworkManagerScript Instance;

	public GameObject piecePrefab;
	public Color[] PlayerColors;
	public Vector2 spawnPoint;

	public Sprite[] characterSprites;
	private MeidoMansionGameManager gameManager;

	void Awake() {
		Instance = this;
	}

	void Start() {
		gameManager = MeidoMansionGameManager.Instance;
	}

	public void SpawnMaster() {
		GameObject pieceObj = GameObject.Instantiate<GameObject>(piecePrefab);
		pieceObj.GetComponent<NetworkPlayerPieceHandler>().pieceName = "MasterP";
		pieceObj.GetComponent<NetworkPlayerPieceHandler>().isMaster = true;
		pieceObj.GetComponent<NetworkPlayerPieceHandler>().SetSpawn(this.spawnPoint);
		pieceObj.GetComponent<NetworkPlayerPieceHandler>().Respawn();
		NetworkServer.Spawn(pieceObj);

		MeidoMansionGameManager.Instance.masterPiece = pieceObj;
	}

	public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId) {
		GameObject playerObj = GameObject.Instantiate<GameObject>(playerPrefab);
		
		playerObj.name = base.numPlayers.ToString();

		GameObject pieceObj = GameObject.Instantiate<GameObject>(piecePrefab);
		playerObj.GetComponent<NetworkPlayerScript>().playerJob = (Jobs)(base.numPlayers);
		playerObj.GetComponent<NetworkPlayerScript>().pieceName = base.numPlayers.ToString();

		//playerObj.GetComponent<Player>().grid = FindObjectOfType<Grid>();
		pieceObj.name = "PlayerPiece"+ base.numPlayers;
		pieceObj.GetComponent<NetworkPlayerPieceHandler>().pieceName = "PlayerPiece"+ base.numPlayers;
		//pieceObj.GetComponentInChildren<SpriteRenderer>().color = PlayerColors[base.numPlayers];
		pieceObj.GetComponentInChildren<SpriteRenderer>().sprite = characterSprites[base.numPlayers];
        NetworkServer.AddPlayerForConnection(conn, playerObj, playerControllerId);
		NetworkServer.Spawn(pieceObj);

		gameManager.AddObject(playerObj);
	}

	public override void OnClientConnect(NetworkConnection conn) {
		//Finds all objects
		base.OnClientConnect(conn);
	}

	public override void OnServerRemovePlayer(NetworkConnection conn, UnityEngine.Networking.PlayerController player) {
		Debug.Log("Removing");
		if(player.gameObject != null) {
			gameManager.Cmd_RemoveObject(player.gameObject);
			Debug.Log("Removings~");
			NetworkServer.Destroy(player.gameObject);
		}
	}
}
