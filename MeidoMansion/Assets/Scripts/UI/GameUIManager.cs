﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class GameUIManager : MonoBehaviour{

	public MeidoMansionGameManager gameManager;
	public static GameUIManager Instance;
	public GameObject startButton;
	public GameObject killButton;
	public GameObject masterIsDead;
	public Grid grid;
	public Text turnCounter;

	void Awake() {
		Instance = this;
	}

	void Start() {
		gameManager = MeidoMansionGameManager.Instance;
		grid = GameObject.Find("Grid").GetComponent<Grid>();
	}

	// Update is called once per frame
	void Update () {
		if(gameManager.masterState == MasterStates.DEAD && !masterIsDead.activeSelf){
			masterIsDead.SetActive(true);
		}
		SetTurn(grid.player.turn);
	}
	public void SetTurn(int turn){
		turnCounter.text = "Turn: " + string.Format("{0}/24", turn);
	}
		
	public void Killmaster(){
		if(grid.player.isKiller){
			if(grid.player.pieceComponent.TryKill()){
				grid.player.Cmd_KillMaster();
			}
			else{
				grid.player.SetTrap();
			}
		}

	}

	public void SetKillerUIVisibility(bool isVisible) {
		killButton.SetActive(isVisible);
	}
}
