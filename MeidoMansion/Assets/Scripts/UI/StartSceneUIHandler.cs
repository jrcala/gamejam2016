﻿using UnityEngine;
using System.Collections;

public class StartSceneUIHandler : MonoBehaviour {

	private MeidoMansionNetworkManagerScript networkManager;
	public string ipString = "";

	// Use this for initialization
	void Start () {
		networkManager = FindObjectOfType<MeidoMansionNetworkManagerScript>();
	}
	
	public void changeIPString(string newValue) {
		ipString = newValue;
		networkManager.networkAddress = ipString;
	}

	public void StartMeidoServer() {
		networkManager.StartServer();
	}

	public void ConnectClient() {
		networkManager.StartClient();
	}


}
