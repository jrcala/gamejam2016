﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainGameUIScript : MonoBehaviour {

	public static MainGameUIScript Instance;

	public GameObject SplashScreen;

	public GameObject ConnectionButtons;
	public GameObject WaitingButtons;

	public GameObject StartGameButton;
	public GameObject WaitTextButton;

	public InputField ipAddess;

	public void Awake() {
		Instance = this;
	}

	public void StartHost() {
		MeidoMansionNetworkManagerScript.Instance.StartHost();
		ConnectionButtons.SetActive(false);
		WaitingButtons.SetActive(true);
	}

	public void ConnectToHost() {
		MeidoMansionNetworkManagerScript.Instance.networkAddress = ipAddess.text;
		MeidoMansionNetworkManagerScript.Instance.StartClient();

		if(MeidoMansionGameManager.Instance == null || !MeidoMansionGameManager.Instance.isServer) {
			Waiting();
		}

		ConnectionButtons.SetActive(false);
		WaitingButtons.SetActive(true);
	}

	public void GameStart() {
		MeidoMansionGameManager.Instance.Cmd_StartGame();
		SplashScreen.SetActive(false);
	}

	public void Waiting() {

	}

	void Update() {
		StartGameButton.SetActive(MeidoMansionGameManager.Instance.isServer && MeidoMansionGameManager.Instance.gameState == GameStates.INITIALIZING);
		WaitTextButton.SetActive(!MeidoMansionGameManager.Instance.isServer && MeidoMansionGameManager.Instance.gameState == GameStates.INITIALIZING);
	}

	public void SetSplashScreenVisibility(bool isVisible) {
		SplashScreen.SetActive(isVisible);
	}

}
