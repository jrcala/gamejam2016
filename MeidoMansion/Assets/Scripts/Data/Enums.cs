﻿using UnityEngine;
using System.Collections;

public enum GameStates {
	NONE = -1,
	MOVE_SELECTION = 0,
	TURN_RESOLVE = 1,
	INITIALIZING = 2,
	STARTING = 3,
	END_DAY = 4
}

public enum TileAnimationStates {
	ANIMATING,
	STATIONARY
}

public enum MasterStates {
	ALIVE,
	DEAD
}

public enum Jobs {
	NONE = -1,
	MAID = 0, 
	CHEF = 1,
	BUTLER = 2,
	GARDENER = 3,
	MASTER = 5
}

public enum MoveDirection {
	NONE = -1, //Not yet selected anything
	UP = 0,
	DOWN = 1,
	LEFT = 2,
	RIGHT = 3,
	STAY = 4, //Stay
}

