﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;

public class Tile : MonoBehaviour {

	public bool canGoLeft = true;
	public bool canGoRight = true;
	public bool canGoDown = true;
	public bool canGoUp = true;
	public bool isGoalTile = false;
	public int xGridPos;
	public int yGridPos;
	public Grid grid;
	public SpriteRenderer selectSprite;
	public SpriteRenderer trapSprite;
	public Text text;
	public bool checkInput = false;
	public bool hasTrap = false;
	public List<int> turnSchedule;


	public void Copy(Tile tile){
		this.canGoLeft = tile.canGoLeft;
		this.canGoRight = tile.canGoRight;
		this.canGoUp = tile.canGoUp;
		this.canGoDown = tile.canGoDown;
		this.isGoalTile = tile.isGoalTile;
		this.xGridPos = tile.xGridPos;
		this.yGridPos = tile.yGridPos;
	}

	void OnDrawGizmos(){
		Gizmos.color = Color.blue;
		Gizmos.color = Color.red;
		if(!canGoLeft){
			Vector3 baseVector = this.transform.position;
			Vector3 startVector = baseVector;
			Vector3 endVector = baseVector;
			startVector.x -= grid.xSpacing/2.2f;
			endVector.x -= grid.xSpacing/2.2f;
			startVector.y -= grid.ySpacing/2.2f;
			endVector.y += grid.ySpacing/2.2f;
			Gizmos.DrawLine(startVector,endVector);
		}
		if(!canGoRight){
			Vector3 baseVector = this.transform.position;
			Vector3 startVector = baseVector;
			Vector3 endVector = baseVector;
			startVector.x += grid.xSpacing/2.2f;
			endVector.x += grid.xSpacing/2.2f;
			startVector.y -= grid.ySpacing/2.2f;
			endVector.y += grid.ySpacing/2.2f;
			Gizmos.DrawLine(startVector,endVector);
		}
		if(!canGoUp){
			Vector3 baseVector = this.transform.position;
			Vector3 startVector = baseVector;
			Vector3 endVector = baseVector;
			startVector.x -= grid.xSpacing/2.2f;
			endVector.x += grid.xSpacing/2.2f;
			startVector.y += grid.ySpacing/2.2f;
			endVector.y += grid.ySpacing/2.2f;
			Gizmos.DrawLine(startVector,endVector);
		}
		if(!canGoDown){
			Vector3 baseVector = this.transform.position;
			Vector3 startVector = baseVector;
			Vector3 endVector = baseVector;
			startVector.x -= grid.xSpacing/2.2f;
			endVector.x += grid.xSpacing/2.2f;
			startVector.y -= grid.ySpacing/2.2f;
			endVector.y -= grid.ySpacing/2.2f;
			Gizmos.DrawLine(startVector,endVector);
		}
	}

	public void AllowInput(){
		checkInput = true;
		selectSprite.gameObject.SetActive(checkInput);
	}
	public void DisableInput(){
		checkInput = false;
		selectSprite.gameObject.SetActive(checkInput);
	}


	public Color GetColor(Color color){
		color.a = selectSprite.color.a;
		return color;
	}
	public void OnMouseEnter(){
		if(checkInput){

			selectSprite.DOColor(GetColor(Color.cyan),0.2f);
		}
	}
	public void OnMouseExit(){

		selectSprite.DOColor(GetColor(Color.white), 0.2f);


	}
		

	public void OnMouseDown(){
		if(checkInput){
			grid.ClickedTile(this);
			selectSprite.DOColor(GetColor(Color.green), 0.1f);
			selectSprite.gameObject.transform.DOPunchScale(Vector2.one *0.1f, 0.5f);
		}
	}
	public void OnMouseUp(){
		selectSprite.DOColor(GetColor(Color.white), 0.1f);
	}

	public List<Tile> TraversableTiles(){
		List<Tile> result = new List<Tile>();
		if(this.canGoLeft && grid.IsValid(this.xGridPos - 1, this.yGridPos)){
			result.Add(grid.GetTile(xGridPos-1, yGridPos));
		}

		if(this.canGoRight && grid.IsValid(this.xGridPos + 1, this.yGridPos)){
			result.Add(grid.GetTile(xGridPos+1, yGridPos));
		}

		if(this.canGoUp && grid.IsValid(this.xGridPos, this.yGridPos-1)){
			result.Add(grid.GetTile(xGridPos, yGridPos-1));
		}

		if(this.canGoDown && grid.IsValid(this.xGridPos, this.yGridPos+1)){
			result.Add(grid.GetTile(xGridPos, yGridPos+1));
		}
		return result;

	}

	public void ShowTrap(){
		trapSprite.gameObject.SetActive(true);
	}
	public void HideTrap(){
		trapSprite.gameObject.SetActive(false);
	}
	public void SetTrap(){
		hasTrap = true;
	}
	public void UnSetTrap(){
		hasTrap = false;
	}
	public void AddTurnSched(int turn){
		text.gameObject.SetActive(true);
		text.text = turn.ToString();
		turnSchedule.Add(turn);
	}
	public void UnSetSched(){
		text.gameObject.SetActive(false);
		turnSchedule.Clear();
	}

	// Use this for initialization
	void Start () {
		trapSprite.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
