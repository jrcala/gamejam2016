﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Grid : MonoBehaviour {

	public int width;
	public int height;
	public float xSpacing;
	public float ySpacing;
	public List<Tile> tileArr = new List<Tile>();

	public Tile tilePrefab;
	public NetworkPlayerScript player = null;
	// Use this for initialization
	[ContextMenu("SetupTiles")]
	void SetupTiles() {

		tileArr.Clear();
		for(int x = 0 ;x < width; x++){
			for(int y = 0; y < height; y++){
				Tile tileGo = GameObject.Instantiate<Tile>(tilePrefab);
				tileGo.grid = this;
				tileGo.xGridPos = x;
				tileGo.yGridPos = y;
				tileArr.Add(tileGo);
				tileGo.transform.parent = this.transform;
				tileGo.transform.localPosition = new Vector2(xSpacing*x, -ySpacing*y);
			}
		}
	}
	[ContextMenu("RefreshTiles")]
	void RefreshTiles(){
		for(int x = 0 ;x < width; x++){
			for(int y = 0; y < height; y++){
				Tile tileGo = GameObject.Instantiate<Tile>(tilePrefab);
				tileGo.Copy(tileArr[x*width+y]);
				tileGo.grid = this;
				Tile toDestroy = tileArr[x*width+y];
				tileArr[x*width+y] = tileGo;
				GameObject.DestroyImmediate(toDestroy.gameObject);
				tileGo.transform.parent = this.transform;
				tileGo.transform.localPosition = new Vector2(xSpacing*x, -ySpacing*y);
			}
		}
	}

	public bool IsValid (int x, int y)
	{
		return (x >= 0 && x < width) && (y >= 0 && y < height);
	}

	public bool HasTargetPlayer() {
		if(player == null) {
			return false;
		}
		else {
			return true;
		}
	}

	public Tile GetTile(int x, int y){
		if(!IsValid(x,y)) return null;
		return tileArr[x*width+y];
	}

	public Vector2 GetTilePos(int x, int y){
		Tile targetTile = GetTile(x,y);

		return targetTile.transform.position;
	}

	public void SetTargetPlayer(NetworkPlayerScript player){
		this.player = player;
	}

	public void Refresh(){
		foreach(Tile tile in tileArr){
			tile.DisableInput();
		}

		if(player != null && player.pieceComponent != null)
			SetTargetTiles(player.pieceComponent.xGridPos, player.pieceComponent.yGridPos);
	}

	void SetTargetTiles(int x, int y){
		Tile rootTile = GetTile(x,y);
		rootTile.AllowInput();
		for(int i = 0; i < 3; i++){
			Tile xTile = GetTile(x+i-1,y);
			Tile yTile = GetTile(x,y+i-1);

			if(xTile != null && (rootTile.canGoLeft && i == 0 || rootTile.canGoRight && i==2)){
				xTile.AllowInput();
			}
			if(yTile != null && (rootTile.canGoUp && i == 0 || rootTile.canGoDown && i==2)){
				yTile.AllowInput();
			}
		}
	}
	public void ClickedTile(Tile tile){
		int tileX = tile.xGridPos;
		int tileY = tile.yGridPos;
		int playerX = player.pieceComponent.xGridPos;
		int playerY = player.pieceComponent.yGridPos;

		if(playerX < tileX){
			player.MovePlayer(MoveDirection.RIGHT);
		}
		else if(playerX > tileX){
			player.MovePlayer(MoveDirection.LEFT);
		}
		else if(playerY > tileY){
			player.MovePlayer(MoveDirection.UP);
		}
		else if(playerY < tileY){
			player.MovePlayer(MoveDirection.DOWN);
		}
		else{
			player.MovePlayer(MoveDirection.STAY);
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
