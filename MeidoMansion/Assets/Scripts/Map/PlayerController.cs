﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : NetworkBehaviour {
	private NetworkPlayerPieceHandler player;

	/// <summary>
	/// The direction the player will move next turn
	/// </summary>
	[SyncVar]
	public MoveDirection playerMove = MoveDirection.NONE;

	public string moveTxt;

	public int moveIndex = 0;

	public List<MoveDirection> moveDirections;

	// Use this for initialization
	void Start () {
		player = this.GetComponent<NetworkPlayerPieceHandler>();

		if (player.isMaster) {
			ParseMove(moveTxt);
		}
	}

	public void Reset(){
		moveIndex = 0;
		if(MeidoMansionGameManager.Instance.masterState == MasterStates.ALIVE){
			player.Respawn();
		}
	}

	public void ParseMove(string move) {
		string[] splMove = move.Split(',');

		for(int i = 0; i < splMove.Length; i++) {
			moveDirections.Add(ConvertCharToMove(splMove[i]));
		}
	}

	public MoveDirection ConvertCharToMove(string charToConvert) {
		if(charToConvert == "u") {
			return MoveDirection.UP;
		}
		else if (charToConvert == "d") {
			return MoveDirection.DOWN;
		}
		else if (charToConvert == "l") {
			return MoveDirection.LEFT;
		}
		else if (charToConvert == "r") {
			return MoveDirection.RIGHT;
		}
		else if (charToConvert == "s") {
			return MoveDirection.STAY;
		}
		return MoveDirection.NONE;
	}

	public MoveDirection GetCurrentMove() {
		return moveDirections[moveIndex];
	}

	public void Rpc_Move() {
		playerMove = moveDirections[moveIndex];
		Tile tile = player.GetCurrentTile();

		if (!tile.hasTrap) {
			tile.isGoalTile = false;
			switch (playerMove) {
				case MoveDirection.UP:
					player.MoveUp();
					break;
				case MoveDirection.DOWN:
					player.MoveDown();
					break;
				case MoveDirection.LEFT:
					player.MoveLeft();
					break;
				case MoveDirection.RIGHT:
					player.MoveRight();
					break;

			}
			moveIndex++;

			if (moveIndex == moveDirections.Count) {
				moveIndex = 0;
			}

			tile = player.GetCurrentTile();
			tile.isGoalTile = true;
		}
		else {
			MeidoMansionGameManager.Instance.Cmd_KillMaster();
		}
	}

	public void Rpc_Move(MoveDirection m) {
		Tile tile = player.GetCurrentTile();
		switch (m) {
			case MoveDirection.UP:
				player.MoveUp();
				break;
			case MoveDirection.DOWN:
				player.MoveDown();
				break;
			case MoveDirection.LEFT:
				player.MoveLeft();
				break;
			case MoveDirection.RIGHT:
				player.MoveRight();
				break;
			case MoveDirection.STAY:
				player.Stay();
				break;
		}
		//playerMove = MoveDirection.NONE;
	}

	[Command]
	public void Cmd_MovePlayer(MoveDirection direction) {
		playerMove = direction;
		MeidoMansionGameManager.Instance.Cmd_CheckAllPlayerMove();
	}

	[Command]
	public void Cmd_PlayerMoveDone() {
		MeidoMansionGameManager.Instance.Cmd_CheckAllPlayerAnimationDone();
	}
}
