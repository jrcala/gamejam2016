﻿
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using DG.Tweening;

public class Player : NetworkBehaviour {
	public Grid grid;

	[SyncVar]
	public int xGridPos;
	[SyncVar]
	public int yGridPos;

	public float moveDuration = 1.0f;

	private PlayerController contrller;

	void Start(){
		contrller = GetComponent<PlayerController>();
		if(grid == null) {
			grid = FindObjectOfType<Grid>();
		}
		this.transform.position = grid.GetTilePos(xGridPos,yGridPos);
	}

	bool TryKill(){
		Tile currTile = GetCurrentTile();
		return currTile.isGoalTile;
	}

	Tile GetCurrentTile(){
		return grid.GetTile(xGridPos,yGridPos);
	}

	void Update() {
	
	}

	public void MoveTo(int x, int y){
			if (grid.IsValid(x, y)) {
				if (x < xGridPos) {
					if (!grid.GetTile(xGridPos, yGridPos).canGoLeft) {
						return;
					}
				}

				if (x > xGridPos) {
					if (!grid.GetTile(xGridPos, yGridPos).canGoRight) {
						return;
					}
				}

				if (y < yGridPos) {
					if (!grid.GetTile(xGridPos, yGridPos).canGoUp) {
						return;
					}
				}

				if (y > yGridPos) {
					if (!grid.GetTile(xGridPos, yGridPos).canGoDown) {
						return;
					}
				}
				Vector2 tilePos = grid.GetTilePos(x, y);
				this.transform.DOMove(tilePos, moveDuration).OnComplete(delegate () {
				});
				xGridPos = x;
				yGridPos = y;
			}
	}
		
	[ContextMenu("Up")]
	public void MoveUp(){
		MoveTo(xGridPos, yGridPos-1);
	}

	[ContextMenu("Left")]
	public void MoveLeft(){
		MoveTo(xGridPos-1, yGridPos);
	}

	[ContextMenu("Right")]
	public void MoveRight(){
		MoveTo(xGridPos +1, yGridPos);
	}

	[ContextMenu("Down")]
	public void MoveDown(){
		MoveTo(xGridPos, yGridPos + 1);
	}
		
}
