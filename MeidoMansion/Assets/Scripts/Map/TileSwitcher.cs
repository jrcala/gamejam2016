﻿using UnityEngine;
using System.Collections;

public class TileSwitcher : MonoBehaviour {
	public enum TileBackground{
		FLOOR,
		GRASS
	}
	public TileBackground tileBackground = TileBackground.GRASS;
	public static int count = 0;

	public Sprite floorTile;
	public Sprite floorTile2;
	public Sprite grassTile;
	public SpriteRenderer targetRenderer;


	[ContextMenu("Refresh")]
	public void Refresh(){
		switch(tileBackground){
			
			case TileBackground.FLOOR:
			targetRenderer.sprite = count%2==0?floorTile:floorTile2;
			count++;
			break;
			case TileBackground.GRASS:
			targetRenderer.sprite = grassTile;
			break;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
