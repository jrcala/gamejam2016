﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ParentTile : MonoBehaviour {

	public bool canGoLeft = true;
	public bool canGoRight = true;
	public bool canGoDown = true;
	public bool canGoUp = true;
	public bool isGoalTile = false;
	public int xGridPos;
	public int yGridPos;
	public ParentGrid grid;
	public SpriteRenderer selectSprite;
	public SpriteRenderer trapSprite;
	public Text text;
	public bool checkInput = false;
	public bool hasTrap = false;
	public List<int> turnSchedule;
	public List<Tile> children = new List<Tile>();


	public void Copy(ParentTile tile){
		this.canGoLeft = tile.canGoLeft;
		this.canGoRight = tile.canGoRight;
		this.canGoUp = tile.canGoUp;
		this.canGoDown = tile.canGoDown;
		this.isGoalTile = tile.isGoalTile;
		this.xGridPos = tile.xGridPos;
		this.yGridPos = tile.yGridPos;
	}

	public void AddChild(Tile child){
		children.Add(child);
	}
	public void ClearChildren(){
		children.Clear();
	}

	void OnDrawGizmos(){
		Gizmos.color = Color.blue;
		Gizmos.color = Color.red;
		if(!canGoLeft){
			Vector3 baseVector = this.transform.position;
			Vector3 startVector = baseVector;
			Vector3 endVector = baseVector;
			startVector.x -= grid.xSpacing/2.2f;
			endVector.x -= grid.xSpacing/2.2f;
			startVector.y -= grid.ySpacing/2.2f;
			endVector.y += grid.ySpacing/2.2f;
			Gizmos.DrawLine(startVector,endVector);
		}
		if(!canGoRight){
			Vector3 baseVector = this.transform.position;
			Vector3 startVector = baseVector;
			Vector3 endVector = baseVector;
			startVector.x += grid.xSpacing/2.2f;
			endVector.x += grid.xSpacing/2.2f;
			startVector.y -= grid.ySpacing/2.2f;
			endVector.y += grid.ySpacing/2.2f;
			Gizmos.DrawLine(startVector,endVector);
		}
		if(!canGoUp){
			Vector3 baseVector = this.transform.position;
			Vector3 startVector = baseVector;
			Vector3 endVector = baseVector;
			startVector.x -= grid.xSpacing/2.2f;
			endVector.x += grid.xSpacing/2.2f;
			startVector.y += grid.ySpacing/2.2f;
			endVector.y += grid.ySpacing/2.2f;
			Gizmos.DrawLine(startVector,endVector);
		}
		if(!canGoDown){
			Vector3 baseVector = this.transform.position;
			Vector3 startVector = baseVector;
			Vector3 endVector = baseVector;
			startVector.x -= grid.xSpacing/2.2f;
			endVector.x += grid.xSpacing/2.2f;
			startVector.y -= grid.ySpacing/2.2f;
			endVector.y -= grid.ySpacing/2.2f;
			Gizmos.DrawLine(startVector,endVector);
		}
	}

	public void AllowInput(){
		checkInput = true;
		selectSprite.gameObject.SetActive(checkInput);
	}
	public void DisableInput(){
		checkInput = false;
		selectSprite.gameObject.SetActive(checkInput);
	}

	public void OnMouseDown(){
		if(checkInput){
			grid.ClickedTile(this);
		}
	}

	public List<ParentTile> TraversableTiles(){
		List<ParentTile> result = new List<ParentTile>();
		if(this.canGoLeft && grid.IsValid(this.xGridPos - 1, this.yGridPos)){
			result.Add(grid.GetTile(xGridPos-1, yGridPos));
		}

		if(this.canGoRight && grid.IsValid(this.xGridPos + 1, this.yGridPos)){
			result.Add(grid.GetTile(xGridPos+1, yGridPos));
		}

		if(this.canGoUp && grid.IsValid(this.xGridPos, this.yGridPos-1)){
			result.Add(grid.GetTile(xGridPos, yGridPos-1));
		}

		if(this.canGoDown && grid.IsValid(this.xGridPos, this.yGridPos+1)){
			result.Add(grid.GetTile(xGridPos, yGridPos+1));
		}
		return result;

	}

	public void ShowTrap(){
		trapSprite.gameObject.SetActive(true);
	}
	public void HideTrap(){
		trapSprite.gameObject.SetActive(false);
	}
	public void SetTrap(){
		hasTrap = true;
	}
	public void UnSetTrap(){
		hasTrap = false;
	}
	public void AddTurnSched(int turn){
		text.gameObject.SetActive(true);
		text.text = turn.ToString();
		turnSchedule.Add(turn);
	}

	// Use this for initialization
	void Start () {
//		trapSprite.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
